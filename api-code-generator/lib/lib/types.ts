
export interface CodegenOptions {
  swagger: Swagger;
  template: Template;
  mustache?: string;
  beautify?: boolean;
  moduleName: string;
  className: string;
  imports: string[];
  isES6?: boolean;
}
export interface CodegenOptionsInfo{
  description: string;
}

export interface SwaggerDefinition {
  description: string;
  required: boolean;
  schema: any;
  $ref: any;
  enum: string[];
  type: string;
  items: SwaggerDefinition;
  minItems: number;
  allOf: any[];
  properties: any;
  tsType?: TsType;
}

export interface Template {
  class: string;
  method: string;
  type: string;
  definitions?: string;
  default_requests?: string;
}

export interface Swagger {
  swagger: string;
  info: CodegenOptionsInfo;
  securityDefinitions?: any;
  schemes: any[];
  host: string;
  basePath: string;
  definitions: {[key: string]: SwaggerDefinition};
  paths: {[key: string]: OpenApiPath};
  security: any;
  produces: any;
  consumes: any;
  parameters?: any[];
}

export interface OpenApiPath {
  deprecated?: boolean;
  security: any;
  responses: string[];
  operationId: string;
  description?: string;
  summary: string;
  externalDocs?: any;
  produces: string[];
  consumes: string[];
  parameters: OpenApiParameter[];
}

export interface OpenApiParameter extends SwaggerDefinition {
  name: string;
  in: string;
  camelCaseName?: string;
  isSingleton?: boolean;
  singleton?: any;
  isBodyParameter?: boolean;
  isPathParameter?: boolean;
  isPatternType?: boolean;
  isQueryParameter?: boolean;
  isHeaderParameter?: boolean;
  isFormParameter?: boolean;
  isBlob: boolean;
  pattern?: any;
  tsType?: any;
  cardinality: any;
}

export interface TsType extends TypeSpec{

}

export interface TypeSpec {
  description: string;
  isEnum: boolean;
  isNullable: boolean;
  tsType?: string;
  isAtomic?: boolean;
  target?: string;
  elementType?: TypeSpec;
  properties?: TypeSpec[];
  name?: string;
  isRef?: boolean;
  isObject?: boolean;
  isArray?: boolean;
  path: string;
  cardinality: string;
}

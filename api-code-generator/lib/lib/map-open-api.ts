import * as _ from 'lodash';
import * as ts from './typescript';
import { capitalizeFirstLetter, clearObjectName, enumTypes } from './typescript';
import { CodegenOptions, OpenApiParameter, OpenApiPath, SwaggerDefinition } from './types';

const defaultSuccessfulResponseType = 'void';

const normalizeName = function (id) {
  return id.replace(/\.|\-|\{|\}/g, '_');
};

const getPathToMethodName = (opts: CodegenOptions, methodName: string, path: string) => {
  if (path === '/' || path === '') {
    return methodName;
  }
  const segments: string =
    path
    // clean url path for requests ending with '/'
      .replace(/\/$/, '')
      .split('/')
      .slice(1)
      .map((segment) => {
        if (segment[0] === '{' && segment[segment.length - 1] === '}') {
          segment = 'by' + segment[1].toUpperCase() + segment.substring(2, segment.length - 1);
        }
        return segment;
      })
      .join('-');
  const result = _.camelCase(segments);
  return methodName.toLowerCase() + result[0].toUpperCase() + result.substring(1);
};

export function mapOpenApi(opts: CodegenOptions) {
  const swagger = opts.swagger;
  const authorizedMethods = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'COPY', 'HEAD', 'OPTIONS', 'LINK', 'UNLIK', 'PURGE', 'LOCK', 'UNLOCK', 'PROPFIND'];
  const data: any = {
    isES6: opts.isES6,
    description: swagger.info.description,
    isSecure: swagger.securityDefinitions !== undefined,
    moduleName: opts.moduleName,
    className: opts.className,
    imports: opts.imports,
    domain: (swagger.schemes && swagger.schemes.length > 0 && swagger.host && swagger.basePath)
      ? swagger.schemes[0] + '://' + swagger.host + swagger.basePath.replace(/\/+$/g, '') : '',
    methods: [],
    definitions: [],
    types: [],
  };

  Object.keys(swagger.definitions)
    .forEach((key) => {
      const definition: SwaggerDefinition = swagger.definitions[key];
      key = clearObjectName(key);
      data.definitions.push({
        name: key,
        description: definition.description,
        tsType: ts.convertType(definition, swagger, key)
      });
    });

  Object.keys(swagger.paths)
    .forEach((path: string) => {
      const api = swagger.paths[path];
      let globalParams = [];
      /**
       * @param {string} key - HTTP method name - eg: 'get', 'post', 'put', 'delete'
       */
      Object.keys(api).forEach((key: string) => {
        if (key.toLowerCase() === 'parameters') {
          globalParams = api[key];
        }
      });
      Object.keys(api).forEach((httpMethodName: string) => {
        const pathApi: OpenApiPath = api[httpMethodName];
        const M = httpMethodName.toUpperCase();
        if (M === '' || authorizedMethods.indexOf(M) === -1) {
          return;
        }

        // Ignore deprecated endpoints
        if (pathApi.deprecated) {
          return;
        }

        const secureTypes = [];
        if (swagger.securityDefinitions !== undefined || pathApi.security !== undefined) {
          const mergedSecurity =
            _.merge([], swagger.security, pathApi.security)
              .map(function (security) {
                return Object.keys(security);
              });
          if (swagger.securityDefinitions) {
            Object.keys(swagger.securityDefinitions).forEach((securityDefinition) => {
              if (mergedSecurity.join(',').indexOf(securityDefinition) !== -1) {
                secureTypes.push(swagger.securityDefinitions[securityDefinition].type);
              }
            });
          }
        }

        let successfulResponseType;
        try {
          const convertedType = ts.convertType(pathApi.responses['200'], null, '200');
          successfulResponseType = convertedType.target || convertedType.tsType || defaultSuccessfulResponseType;
        } catch (error) {
          successfulResponseType = defaultSuccessfulResponseType;
        }
        successfulResponseType = clearObjectName(successfulResponseType);

        const method = {
          path: path,
          pathFormatString: path.replace(/{/g, '${parameters.'),
          expressPath: path,
          className: opts.className,
          methodName: pathApi.operationId ? normalizeName(pathApi.operationId) : getPathToMethodName(opts, httpMethodName, path),
          pathMethodName: getPathToMethodName(opts, httpMethodName, path),
          method: M,
          isGET: M === 'GET',
          isPOST: M === 'POST',
          summary: pathApi.description || pathApi.summary,
          externalDocs: pathApi.externalDocs,
          isSecure: swagger.security !== undefined || pathApi.security !== undefined,
          isSecureToken: secureTypes.indexOf('oauth2') !== -1,
          isSecureApiKey: secureTypes.indexOf('apiKey') !== -1,
          isSecureBasic: secureTypes.indexOf('basic') !== -1,
          parameters: [],
          headers: [],
          hasFormParameters: false,
          headersForMockServer: [],
          successfulResponseType,
          responseIsBlob: successfulResponseType === 'Blob'
        };

        if (method.isSecure && method.isSecureToken) {
          data.isSecureToken = method.isSecureToken;
        }
        if (method.isSecure && method.isSecureApiKey) {
          data.isSecureApiKey = method.isSecureApiKey;
        }
        if (method.isSecure && method.isSecureBasic) {
          data.isSecureBasic = method.isSecureBasic;
        }
        const produces = pathApi.produces || swagger.produces;
        if (produces) {
          method.headers.push({
            name: 'Accept',
            value: `'${produces.map(function (value) {
              return value;
            }).join(', ')}'`,
          });
          method.headersForMockServer.push(
            {name: 'Content-Type', value: '\'' + produces[0] + '\''}
          );
        }

        const consumes = pathApi.consumes || swagger.consumes;
        if (consumes) {
          const preferredContentType = consumes[0] || '';
          method.headers.push({name: 'Content-Type', value: '\'' + preferredContentType + '\''});
        }

        let params: OpenApiParameter[] = [];
        if (_.isArray(pathApi.parameters)) {
          params = pathApi.parameters;
        }
        params = params.concat(globalParams);
        params.forEach((parameter) => {
          // Ignore parameters which contain the x-exclude-from-bindings extension
          if (parameter['x-exclude-from-bindings'] === true) {
            return;
          }

          // Ignore headers which are injected by proxies & app servers
          // eg: https://cloud.google.com/appengine/docs/go/requests#Go_Request_headers
          if (parameter['x-proxy-header']) {
            return;
          }
          if (_.isString(parameter.$ref)) {
            const segments = parameter.$ref.split('/');
            parameter = swagger.parameters[segments.length === 1 ? segments[0] : segments[2]];
          }
          parameter.camelCaseName = _.camelCase(parameter.name);
          // solved in interceptor
          if (parameter.camelCaseName === 'xBrowserFingerPrint') {
            return;
          }
          // solved in interceptor
          if (parameter.camelCaseName === 'xXsrfToken') {
            return;
          }

          if (parameter['x-exclude-from-bindings'] === true) {
            return;
          }
          if (parameter.enum && parameter.enum.length === 1) {
            parameter.isSingleton = true;
            parameter.singleton = parameter.enum[0];
          }
          if (parameter.in === 'body') {
            parameter.isBodyParameter = true;
          } else if (parameter.in === 'path') {
            parameter.isPathParameter = true;
          } else if (parameter.in === 'query') {
            if (parameter['x-name-pattern']) {
              parameter.isPatternType = true;
              parameter.pattern = parameter['x-name-pattern'];
            }
            parameter.isQueryParameter = true;
          } else if (parameter.in === 'header') {
            parameter.isHeaderParameter = true;
          } else if (parameter.in === 'formData') {
            parameter.isFormParameter = true;
            method.hasFormParameters = true;
          }
          parameter.tsType = ts.convertType(parameter as SwaggerDefinition, null, method.pathMethodName + capitalizeFirstLetter(parameter.name));
          parameter.isBlob = parameter.tsType.target === 'Blob';
          parameter.cardinality = parameter.required ? '' : '?';
          method.parameters.push(parameter);
        });

        method.parameters.forEach((parameter: any) => {
          method.expressPath = method.expressPath.replace(new RegExp(`{${parameter.name}}`, 'g'), `:${parameter.name}`);
        });

        data.methods.push(method);
      });
    });

  Object.keys(enumTypes).forEach((key) => {
    data.types.push({
      name: enumTypes[key],
      description: '',
      tsType: {
        description: '',
        isEnum: true,
        isNullable: false,
        target: key,
        isRef: true,
      }
    });
  });

  data.definitions = data.definitions.filter((definition: SwaggerDefinition) => {
    // enums are rendered by enumTypes with original name
    // -> enum need to be rendered as type, not as interface
    return !definition.tsType.isEnum;
  });

  return data;
}

import * as _ from 'lodash';
import * as Mustache from 'mustache';
import * as prettier from 'prettier';
import { mapOpenApi } from './map-open-api';
import { CodegenOptions } from './types';


export class CodeGen {
  public static getCustomCode(opts: CodegenOptions, rootTemplateName: string) {
    if (opts.swagger.swagger !== '2.0') {
      throw new Error('Only Swagger 2 specs are supported');
    }

    if (!_.isObject(opts.template) || !_.isString(opts.template.class) || !_.isString(opts.template.method)) {
      throw new Error('Unprovided custom template. Please use the following template: template: { class: "...", method: "...", request: "..." }');
    }
    const data = mapOpenApi(opts);
    if (opts.mustache) {
      _.assign(data, opts.mustache);
    }

    // Ensure we don't encode special characters
    (Mustache as any).escape = (value) => value;

    const source = Mustache.render(opts.template[rootTemplateName], data, opts.template);
    if (opts.beautify === undefined || opts.beautify === true) {
      // return source;
      try {
        return prettier.format(source, {parser: 'typescript', 'singleQuote': true});
      } catch (e) {
        return source;
      }
    } else {
      return source;
    }
  }
}

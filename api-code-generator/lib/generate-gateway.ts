import * as fs from 'fs';
import { CodeGen } from './lib/codegen';
import { CodegenOptions } from './lib/types';
import { ProcessSettings, processSettings } from './process-settings';

function generate(settings: ProcessSettings) {
    const options: CodegenOptions = {
        moduleName: 'gateway',
        className: 'GatewayService',
        swagger: settings.apiSpec,
        imports: [],
        template: {
            class: fs.readFileSync(settings.directoryPath + '/../templates/gateway/class.mustache', 'utf-8'),
            method: fs.readFileSync(settings.directoryPath + '/../templates/gateway/method.mustache', 'utf-8'),
            type: fs.readFileSync(settings.directoryPath + '/../templates/shared/type.mustache', 'utf-8'),
            definitions: fs.readFileSync(settings.directoryPath + '/../templates/shared/definitions.mustache', 'utf-8'),
        },
    };

    fs.writeFileSync(settings.projectPath + settings.gatewayOutputFolder + 'gateway.service.ts', CodeGen.getCustomCode(options, 'class'));
    fs.writeFileSync(settings.projectPath + settings.gatewayOutputFolder + 'api-model.ts', CodeGen.getCustomCode(options, 'definitions'));
}

processSettings(generate);

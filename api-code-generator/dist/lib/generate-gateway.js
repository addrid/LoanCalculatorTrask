"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var codegen_1 = require("./lib/codegen");
var process_settings_1 = require("./process-settings");
function generate(settings) {
    var options = {
        moduleName: 'gateway',
        className: 'GatewayService',
        swagger: settings.apiSpec,
        imports: [],
        template: {
            class: fs.readFileSync(settings.directoryPath + '/../templates/gateway/class.mustache', 'utf-8'),
            method: fs.readFileSync(settings.directoryPath + '/../templates/gateway/method.mustache', 'utf-8'),
            type: fs.readFileSync(settings.directoryPath + '/../templates/shared/type.mustache', 'utf-8'),
            definitions: fs.readFileSync(settings.directoryPath + '/../templates/shared/definitions.mustache', 'utf-8'),
        },
    };
    fs.writeFileSync(settings.projectPath + settings.gatewayOutputFolder + 'gateway.service.ts', codegen_1.CodeGen.getCustomCode(options, 'class'));
    fs.writeFileSync(settings.projectPath + settings.gatewayOutputFolder + 'api-model.ts', codegen_1.CodeGen.getCustomCode(options, 'definitions'));
}
process_settings_1.processSettings(generate);
//# sourceMappingURL=generate-gateway.js.map
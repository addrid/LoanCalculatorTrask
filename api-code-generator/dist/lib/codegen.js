"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var Mustache = require("mustache");
var prettier = require("prettier");
var map_open_api_1 = require("./map-open-api");
var CodeGen = /** @class */ (function () {
    function CodeGen() {
    }
    CodeGen.getCustomCode = function (opts, rootTemplateName) {
        if (opts.swagger.swagger !== '2.0') {
            throw new Error('Only Swagger 2 specs are supported');
        }
        if (!_.isObject(opts.template) || !_.isString(opts.template.class) || !_.isString(opts.template.method)) {
            throw new Error('Unprovided custom template. Please use the following template: template: { class: "...", method: "...", request: "..." }');
        }
        var data = map_open_api_1.mapOpenApi(opts);
        if (opts.mustache) {
            _.assign(data, opts.mustache);
        }
        // Ensure we don't encode special characters
        Mustache.escape = function (value) { return value; };
        var source = Mustache.render(opts.template[rootTemplateName], data, opts.template);
        if (opts.beautify === undefined || opts.beautify === true) {
            // return source;
            try {
                return prettier.format(source, { parser: 'typescript', 'singleQuote': true });
            }
            catch (e) {
                return source;
            }
        }
        else {
            return source;
        }
    };
    return CodeGen;
}());
exports.CodeGen = CodeGen;
//# sourceMappingURL=codegen.js.map
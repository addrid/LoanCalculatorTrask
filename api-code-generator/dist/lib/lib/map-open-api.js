"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var ts = require("./typescript");
var typescript_1 = require("./typescript");
var defaultSuccessfulResponseType = 'void';
var normalizeName = function (id) {
    return id.replace(/\.|\-|\{|\}/g, '_');
};
var getPathToMethodName = function (opts, methodName, path) {
    if (path === '/' || path === '') {
        return methodName;
    }
    var segments = path
        .replace(/\/$/, '')
        .split('/')
        .slice(1)
        .map(function (segment) {
        if (segment[0] === '{' && segment[segment.length - 1] === '}') {
            segment = 'by' + segment[1].toUpperCase() + segment.substring(2, segment.length - 1);
        }
        return segment;
    })
        .join('-');
    var result = _.camelCase(segments);
    return methodName.toLowerCase() + result[0].toUpperCase() + result.substring(1);
};
function mapOpenApi(opts) {
    var swagger = opts.swagger;
    var authorizedMethods = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'COPY', 'HEAD', 'OPTIONS', 'LINK', 'UNLIK', 'PURGE', 'LOCK', 'UNLOCK', 'PROPFIND'];
    var data = {
        isES6: opts.isES6,
        description: swagger.info.description,
        isSecure: swagger.securityDefinitions !== undefined,
        moduleName: opts.moduleName,
        className: opts.className,
        imports: opts.imports,
        domain: (swagger.schemes && swagger.schemes.length > 0 && swagger.host && swagger.basePath)
            ? swagger.schemes[0] + '://' + swagger.host + swagger.basePath.replace(/\/+$/g, '') : '',
        methods: [],
        definitions: [],
        types: [],
    };
    Object.keys(swagger.definitions)
        .forEach(function (key) {
        var definition = swagger.definitions[key];
        key = typescript_1.clearObjectName(key);
        data.definitions.push({
            name: key,
            description: definition.description,
            tsType: ts.convertType(definition, swagger, key)
        });
    });
    Object.keys(swagger.paths)
        .forEach(function (path) {
        var api = swagger.paths[path];
        var globalParams = [];
        /**
         * @param {string} key - HTTP method name - eg: 'get', 'post', 'put', 'delete'
         */
        Object.keys(api).forEach(function (key) {
            if (key.toLowerCase() === 'parameters') {
                globalParams = api[key];
            }
        });
        Object.keys(api).forEach(function (httpMethodName) {
            var pathApi = api[httpMethodName];
            var M = httpMethodName.toUpperCase();
            if (M === '' || authorizedMethods.indexOf(M) === -1) {
                return;
            }
            // Ignore deprecated endpoints
            if (pathApi.deprecated) {
                return;
            }
            var secureTypes = [];
            if (swagger.securityDefinitions !== undefined || pathApi.security !== undefined) {
                var mergedSecurity_1 = _.merge([], swagger.security, pathApi.security)
                    .map(function (security) {
                    return Object.keys(security);
                });
                if (swagger.securityDefinitions) {
                    Object.keys(swagger.securityDefinitions).forEach(function (securityDefinition) {
                        if (mergedSecurity_1.join(',').indexOf(securityDefinition) !== -1) {
                            secureTypes.push(swagger.securityDefinitions[securityDefinition].type);
                        }
                    });
                }
            }
            var successfulResponseType;
            try {
                var convertedType = ts.convertType(pathApi.responses['200'], null, '200');
                successfulResponseType = convertedType.target || convertedType.tsType || defaultSuccessfulResponseType;
            }
            catch (error) {
                successfulResponseType = defaultSuccessfulResponseType;
            }
            successfulResponseType = typescript_1.clearObjectName(successfulResponseType);
            var method = {
                path: path,
                pathFormatString: path.replace(/{/g, '${parameters.'),
                expressPath: path,
                className: opts.className,
                methodName: pathApi.operationId ? normalizeName(pathApi.operationId) : getPathToMethodName(opts, httpMethodName, path),
                pathMethodName: getPathToMethodName(opts, httpMethodName, path),
                method: M,
                isGET: M === 'GET',
                isPOST: M === 'POST',
                summary: pathApi.description || pathApi.summary,
                externalDocs: pathApi.externalDocs,
                isSecure: swagger.security !== undefined || pathApi.security !== undefined,
                isSecureToken: secureTypes.indexOf('oauth2') !== -1,
                isSecureApiKey: secureTypes.indexOf('apiKey') !== -1,
                isSecureBasic: secureTypes.indexOf('basic') !== -1,
                parameters: [],
                headers: [],
                hasFormParameters: false,
                headersForMockServer: [],
                successfulResponseType: successfulResponseType,
                responseIsBlob: successfulResponseType === 'Blob'
            };
            if (method.isSecure && method.isSecureToken) {
                data.isSecureToken = method.isSecureToken;
            }
            if (method.isSecure && method.isSecureApiKey) {
                data.isSecureApiKey = method.isSecureApiKey;
            }
            if (method.isSecure && method.isSecureBasic) {
                data.isSecureBasic = method.isSecureBasic;
            }
            var produces = pathApi.produces || swagger.produces;
            if (produces) {
                method.headers.push({
                    name: 'Accept',
                    value: "'" + produces.map(function (value) {
                        return value;
                    }).join(', ') + "'",
                });
                method.headersForMockServer.push({ name: 'Content-Type', value: '\'' + produces[0] + '\'' });
            }
            var consumes = pathApi.consumes || swagger.consumes;
            if (consumes) {
                var preferredContentType = consumes[0] || '';
                method.headers.push({ name: 'Content-Type', value: '\'' + preferredContentType + '\'' });
            }
            var params = [];
            if (_.isArray(pathApi.parameters)) {
                params = pathApi.parameters;
            }
            params = params.concat(globalParams);
            params.forEach(function (parameter) {
                // Ignore parameters which contain the x-exclude-from-bindings extension
                if (parameter['x-exclude-from-bindings'] === true) {
                    return;
                }
                // Ignore headers which are injected by proxies & app servers
                // eg: https://cloud.google.com/appengine/docs/go/requests#Go_Request_headers
                if (parameter['x-proxy-header']) {
                    return;
                }
                if (_.isString(parameter.$ref)) {
                    var segments = parameter.$ref.split('/');
                    parameter = swagger.parameters[segments.length === 1 ? segments[0] : segments[2]];
                }
                parameter.camelCaseName = _.camelCase(parameter.name);
                // solved in interceptor
                if (parameter.camelCaseName === 'xBrowserFingerPrint') {
                    return;
                }
                // solved in interceptor
                if (parameter.camelCaseName === 'xXsrfToken') {
                    return;
                }
                if (parameter['x-exclude-from-bindings'] === true) {
                    return;
                }
                if (parameter.enum && parameter.enum.length === 1) {
                    parameter.isSingleton = true;
                    parameter.singleton = parameter.enum[0];
                }
                if (parameter.in === 'body') {
                    parameter.isBodyParameter = true;
                }
                else if (parameter.in === 'path') {
                    parameter.isPathParameter = true;
                }
                else if (parameter.in === 'query') {
                    if (parameter['x-name-pattern']) {
                        parameter.isPatternType = true;
                        parameter.pattern = parameter['x-name-pattern'];
                    }
                    parameter.isQueryParameter = true;
                }
                else if (parameter.in === 'header') {
                    parameter.isHeaderParameter = true;
                }
                else if (parameter.in === 'formData') {
                    parameter.isFormParameter = true;
                    method.hasFormParameters = true;
                }
                parameter.tsType = ts.convertType(parameter, null, method.pathMethodName + typescript_1.capitalizeFirstLetter(parameter.name));
                parameter.isBlob = parameter.tsType.target === 'Blob';
                parameter.cardinality = parameter.required ? '' : '?';
                method.parameters.push(parameter);
            });
            method.parameters.forEach(function (parameter) {
                method.expressPath = method.expressPath.replace(new RegExp("{" + parameter.name + "}", 'g'), ":" + parameter.name);
            });
            data.methods.push(method);
        });
    });
    Object.keys(typescript_1.enumTypes).forEach(function (key) {
        data.types.push({
            name: typescript_1.enumTypes[key],
            description: '',
            tsType: {
                description: '',
                isEnum: true,
                isNullable: false,
                target: key,
                isRef: true,
            }
        });
    });
    data.definitions = data.definitions.filter(function (definition) {
        // enums are rendered by enumTypes with original name
        // -> enum need to be rendered as type, not as interface
        return !definition.tsType.isEnum;
    });
    return data;
}
exports.mapOpenApi = mapOpenApi;
//# sourceMappingURL=map-open-api.js.map
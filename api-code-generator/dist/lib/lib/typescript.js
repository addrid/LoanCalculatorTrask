'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
var _ = require('lodash');
exports.enumTypes = {};
var typeNames = {};
/**
 * Recursively converts a swagger type description into a typescript type, i.e., a model for our mustache
 * template.
 *
 * Not all type are currently supported, but they should be straightforward to add.
 *
 * @param swaggerType a swagger type definition, i.e., the right hand side of a swagger type definition.
 * @returns a recursive structure representing the type, which can be used as a template model.
 */
function convertType(swaggerType, swagger, name) {
    var typespec = {
        description: swaggerType.description,
        isEnum: false,
        isNullable: !swaggerType.required,
        path: '',
        cardinality: '',
    };
    if (name) {
        typespec.path = name;
    }
    name = clearObjectName(name);
    if (swaggerType.hasOwnProperty('required')) {
        if (util_1.isArray(swaggerType.required)) {
            var _loop_1 = function (propertiesKey) {
                if (!swaggerType.required.some(function (item) {
                    return item === propertiesKey;
                })) {
                    swaggerType.properties[propertiesKey].required = false;
                }
            };
            for (var propertiesKey in swaggerType.properties) {
                _loop_1(propertiesKey);
            }
        }
        else if (!swaggerType.required) {
            typespec.cardinality = '?';
        }
    }
    if (swaggerType.hasOwnProperty('schema')) {
        return convertType(swaggerType.schema, null, 'schema');
    }
    else if (_.isString(swaggerType.$ref)) {
        typespec.tsType = 'ref';
        typespec.target = swaggerType.$ref.substring(swaggerType.$ref.lastIndexOf('/') + 1);
    }
    else if (swaggerType.hasOwnProperty('enum')) {
        typespec.tsType = swaggerType.enum.map(function (str) {
            return JSON.stringify(str);
        }).join(' | ');
        if (exports.enumTypes.hasOwnProperty(typespec.tsType)) {
            // named identical
        }
        else {
            if (typespec.path === name) {
                exports.enumTypes[typespec.tsType] = name;
            }
            else {
                var modifiedName = 'TN' + capitalizeFirstLetter(name);
                if (typeNames.hasOwnProperty(modifiedName)) {
                    // name used, use path
                    exports.enumTypes[typespec.tsType] = 'TP' + capitalizeFirstLetter(typespec.path);
                }
                else {
                    exports.enumTypes[typespec.tsType] = modifiedName;
                    typeNames[modifiedName] = true;
                }
            }
        }
        typespec.tsType = exports.enumTypes[typespec.tsType];
        typespec.isAtomic = true;
        typespec.isEnum = true;
    }
    else if (swaggerType.type === 'string') {
        typespec.tsType = 'string';
    }
    else if (swaggerType.type === 'number' || swaggerType.type === 'integer') {
        typespec.tsType = 'number';
    }
    else if (swaggerType.type === 'boolean') {
        typespec.tsType = 'boolean';
    }
    else if (swaggerType.type === 'file') {
        typespec.tsType = 'Blob';
        typespec.target = 'Blob';
    }
    else if (swaggerType.type === 'array') {
        typespec.elementType = convertType(swaggerType.items, null, typespec.path);
        // TODO: enum in array
        typespec.tsType = "Array<" + (typespec.elementType.target || typespec.elementType.tsType || 'any') + ">";
    }
    else {
        if (swaggerType.minItems >= 0 && swaggerType.hasOwnProperty('title') && !swaggerType.$ref) {
            typespec.tsType = 'any';
        }
        else {
            typespec.tsType = 'object';
            typespec.properties = [];
            if (swaggerType.allOf) {
                _.forEach(swaggerType.allOf, function (ref) {
                    if (ref.$ref) {
                        var refSegments = ref.$ref.split('/');
                        var name_1 = refSegments[refSegments.length - 1];
                        _.forEach(swagger.definitions, function (definition, definitionName) {
                            if (clearObjectName(definitionName) === name_1) {
                                var property = convertType(definition, swagger, typespec.path + capitalizeFirstLetter(clearObjectName(definitionName)));
                                (_a = typespec.properties).push.apply(_a, property.properties);
                            }
                            var _a;
                        });
                    }
                    else {
                        var property = convertType(ref, null, typespec.path + JSON.stringify(ref));
                        (_a = typespec.properties).push.apply(_a, property.properties);
                    }
                    var _a;
                });
            }
            _.forEach(swaggerType.properties, function (propertyType, propertyName) {
                var property = convertType(propertyType, null, typespec.path + capitalizeFirstLetter(propertyName));
                if (propertyName === 'csrfCode') {
                    property.cardinality = '?';
                }
                property.name = clearObjectName(propertyName);
                typespec.properties.push(property);
            });
        }
    }
    // Since Mustache does not provide equality checks, we need to do the case distinction via explicit booleans
    typespec.isRef = typespec.tsType === 'ref';
    typespec.isObject = typespec.tsType === 'object';
    typespec.isArray = typespec.tsType.indexOf('Array<') > -1;
    typespec.isAtomic = typespec.isAtomic || _.includes(['string', 'number', 'boolean', 'any', 'Blob'], typespec.tsType);
    return typespec;
}
exports.convertType = convertType;
function capitalizeFirstLetter(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
}
exports.capitalizeFirstLetter = capitalizeFirstLetter;
function clearObjectName(name) {
    return name
        .replace(new RegExp(',string»', 'g'), '')
        .replace(new RegExp('«', 'g'), '_')
        .replace(new RegExp('»', 'g'), '')
        .replace(new RegExp(',', 'g'), '_');
}
exports.clearObjectName = clearObjectName;
//# sourceMappingURL=typescript.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var mock_gen_1 = require("./lib/mock-gen");
var process_settings_1 = require("./process-settings");
function generate(settings) {
    var options = {
        moduleName: 'mock',
        className: 'MockServer',
        swagger: settings.apiSpec,
        imports: [],
        template: {
            class: fs.readFileSync(settings.directoryPath + '/../templates/mock/class.mustache', 'utf-8'),
            method: fs.readFileSync(settings.directoryPath + '/../templates/mock/method.mustache', 'utf-8'),
            type: fs.readFileSync(settings.directoryPath + '/../templates/mock/type.mustache', 'utf-8'),
            default_requests: fs.readFileSync(settings.directoryPath + '/../templates/mock/default-requests.mustache', 'utf-8')
        },
    };
    fs.writeFileSync(settings.projectPath + settings.mockOutputFolder + 'methods.ts', mock_gen_1.MockGen.generateMockServer(options, 'class'));
    fs.writeFileSync(settings.projectPath + settings.mockOutputFolder + 'requests.ts', mock_gen_1.MockGen.generateMockServer(options, 'default_requests'));
}
process_settings_1.processSettings(generate);
//# sourceMappingURL=generate-mock.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var codegen_1 = require("./lib/codegen");
var process_settings_1 = require("./process-settings");
function generate(settings) {
    var options = {
        moduleName: 'gateway',
        className: 'GatewayService',
        swagger: settings.apiSpec,
        imports: [],
        template: {
            class: fs.readFileSync(settings.directoryPath + '/../templates/api-test/class.mustache', 'utf-8'),
            method: fs.readFileSync(settings.directoryPath + '/../templates/api-test/method.mustache', 'utf-8'),
            type: fs.readFileSync(settings.directoryPath + '/../templates/shared/type.mustache', 'utf-8'),
            definitions: fs.readFileSync(settings.directoryPath + '/../templates/shared/definitions.mustache', 'utf-8'),
        },
    };
    fs.writeFileSync(settings.projectPath + settings.apiTestOutputFolder + 'gateway.service.ts', codegen_1.CodeGen.getCustomCode(options, 'class'));
}
process_settings_1.processSettings(generate);
//# sourceMappingURL=generate-api-test.js.map